# README #

1/3スケールのアナログテレビ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。
背面は情報が少ないため実機と異なると思います。
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

組み込む液晶パネルは以下を想定しています。
Adafrit 913
https://store.shopping.yahoo.co.jp/marutsuelec/574373.html?sc_e=slga_pla

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_old_tv/raw/b00a1038a0c26ec97becc8239caecadd4f0590d3/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-09-25%2001.44.32.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_old_tv/raw/b00a1038a0c26ec97becc8239caecadd4f0590d3/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-09-25%2001.47.40.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_old_tv/raw/b00a1038a0c26ec97becc8239caecadd4f0590d3/ExampleImage.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_old_tv/raw/b00a1038a0c26ec97becc8239caecadd4f0590d3/ExampleImage_2.jpg)
